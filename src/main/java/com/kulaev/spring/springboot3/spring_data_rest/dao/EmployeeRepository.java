package com.kulaev.spring.springboot3.spring_data_rest.dao;
import com.kulaev.spring.springboot3.spring_data_rest.Entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
}
